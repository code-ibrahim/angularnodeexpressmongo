const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();
var corsOption = { origin: "http://localhost:8081" };
app.use(cors(corsOption));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

const db = require("./app/models");
db.mongoose
    .connect(db.url, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => {
        console.log("Connected to the database!!!!");
    })
    .catch(err => {
        console.log("Cannot connect to the database!", err);
        process.exit();
    });

app.get("/", (req, res) => {
    res.json({ message: "welcome to node.js express service..." });
});

require("./app/routes/userProfile.routes")(app);

const PORT = process.env.PORT || 8080;

app.listen(PORT, () => {
    console.log('server is running on port ${PORT}' + PORT);
});


