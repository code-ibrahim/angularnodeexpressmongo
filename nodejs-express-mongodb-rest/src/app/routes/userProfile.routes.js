module.exports = app => {
    const profiles = require("../controllers/profile.controller.js");

    var router = require("express").Router();

    // Create a new Tutorial
    router.post("/", profiles.create);

    // Retrieve all Tutorials
    router.get("/", profiles.findAll);

    // Retrieve all published Tutorials
    router.get("/name", profiles.findByName);

    router.get("/Id", profiles.findById);

    // Retrieve a single Tutorial with id
    router.get("/:id", profiles.findOne);

    // Update a Tutorial with id
    router.put("/:id", profiles.update);

    // Delete a Tutorial with id
    router.delete("/:id", profiles.delete);

    // Create a new Tutorial
    router.delete("/", profiles.deleteAll);

    app.use('/api/profiles', router);
};