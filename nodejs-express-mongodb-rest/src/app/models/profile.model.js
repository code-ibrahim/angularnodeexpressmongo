module.exports = mongoose => {
    const Profile = mongoose.model(
        "profile",
        mongoose.Schema(
            {
                //title:String,
                Image: String,
                name: String,
                //id: String
            },
            { timestamps: true }
        )
    );

    return Profile;
};