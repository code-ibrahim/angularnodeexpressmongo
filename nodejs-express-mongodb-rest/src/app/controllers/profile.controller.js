const db = require("../models");
const Profile = db.profiles;

// Create and Save a new Tutorial
exports.create = (req, res) => {
    // Validate request
    if (!req.body.Image) {
        res.status(400).send({ message: "Content can not be empty!" });
        return;
    }

    // Create a Tutorial
    const profile = new Profile({
        Image: req.body.Image,
        name: req.body.name,
        id: req.body.id
    });

    // Save Tutorial in the database
    profile
        .save(profile)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the Profile."
            });
        });
};

// Retrieve all Tutorials from the database.
exports.findAll = (req, res) => {
    //const Image = req.query.Image;
    //var condition = Image ? { Image: { $regex: new RegExp(Image), $options: "i" } } : {};

    Profile.find({})
        .then(data => {
            if (!data)
                res.status(404).send({ message: "Not found Profiles"});
            else
                res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving profiles."
            });
        });
};

// Find a single Tutorial with an id
exports.findOne = (req, res) => {
    const id = req.params.id;

    Profile.findById(id)
        .then(data => {
            if (!data)
                res.status(404).send({ message: "Not found profile with id " + id });
            else res.send(data);
        })
        .catch(err => {
            res
                .status(500)
                .send({ message: "Error retrieving Profile with id=" + id });
        });
};

// Update a Tutorial by the id in the request
exports.update = (req, res) => {
    if (!req.body) {
        return res.status(400).send({
            message: "Data to update can not be empty!"
        });
    }

    const id = req.params.id;

    Profile.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot update Tutorial with id=${id}. Maybe Profile was not found!`
                });
            } else res.send({ message: "Profile was updated successfully." });
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Profile with id=" + id
            });
        });
};

// Delete a Tutorial with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    Profile.findByIdAndRemove(id)
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot delete Profile with id=${id}. Maybe Tutorial was not found!`
                });
            } else {
                res.send({
                    message: "Profile was deleted successfully!"
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Profile with id=" + id
            });
        });
};

// Delete all Tutorials from the database.
exports.deleteAll = (req, res) => {
    Profile.deleteMany({})
        .then(data => {
            res.send({
                message: `${data.deletedCount} Profiles were deleted successfully!`
            });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while removing all Profiles."
            });
        });
};

// Find all published Tutorials
exports.findByName = (req, res) => {
    Profile.find({ name:"User1" })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving Profiles."
            });
        });
};

exports.findById = (req, res) => {
    Profile.find({ id: "1001" })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving Profiles."
            });
        });
};