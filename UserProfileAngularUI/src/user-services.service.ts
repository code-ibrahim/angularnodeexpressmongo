import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { IUser } from './app/Users';

@Injectable({
  providedIn: 'root'
})
export class UserServicesService {

  constructor(private http: HttpClient) { }

  getUsers(): Observable<IUser[]> {
    console.log("getUsers in user service");
    let temVar = this.http.get<IUser[]>('https://s3-ap-southeast-1.amazonaws.com/he-public-data/users49b8675.json').pipe(catchError(this.errorHandler));;
    console.log("temVar : " + temVar);
    return temVar;
  }

  errorHandler(error: HttpErrorResponse) {
    console.error(error);
    return throwError(error.message || "Server Error");
  } 

}
