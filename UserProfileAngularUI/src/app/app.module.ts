import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponentComponent } from './home-component/home-component.component';
import { LayoutComponentComponent } from './layout-component/layout-component.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { UserServicesService } from '../user-services.service';
import { routing } from './app.routing';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UserRestServiceService } from '../user-rest-service.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponentComponent,
    LayoutComponentComponent,
    UserProfileComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    routing
  ],
  providers: [UserServicesService,
    UserRestServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
