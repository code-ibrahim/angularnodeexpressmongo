import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserRestServiceService } from '../../user-rest-service.service';
import { IUser } from '../Users';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  profiles: IUser[];
  profile: IUser;
  id: string;
  msg: string;

  constructor(private _userRestService: UserRestServiceService, private route: ActivatedRoute) {
    this.id = this.route.snapshot.params['id'];
    this.getUsers();
  }

  ngOnInit(): void {
    //this.id = this.route.snapshot.params['id'];
    //this.getUsers();
  }


  async getUsers() {
    console.log("getUsers clicked");
    this.msg = undefined;
    this.profiles = undefined;
    this._userRestService.getUsersById(this.id).subscribe(
      r => {
        this.profiles = r;
        this.msg = "success";
        console.log("responce : " + r);
      },
      er => {
        this.msg = "error";
        console.log(er);
      },
      () => console.log("getUsers executed")
    );
    let count = 0;
    while (this.profiles == undefined && count < 25) {
      count++;
      await this.delay(1000);
    }
    console.log("count : " + count);
    console.log("users : " + this.profiles);


    if (this.profiles != undefined) {
      for (var i of this.profiles) {
        if (i["Image"] != undefined) {
          console.log(i["Image"]);
          this.profile = i;
        }
        else {
          console.log(" iamega is null ");
        }
      }
    }
    
   
  }


  private delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

}
