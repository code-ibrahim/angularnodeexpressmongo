import { Component, OnInit } from '@angular/core';
import { UserServicesService } from '../../user-services.service';
import { IUser } from '../Users';

@Component({
  selector: 'app-home-component',
  templateUrl: './home-component.component.html',
  styleUrls: ['./home-component.component.css']
})
export class HomeComponentComponent implements OnInit {

  users: IUser[];
  msg: string;
  images: string[] = [];

  constructor(private _userService: UserServicesService) {
    this.getUsers();
  }

  ngOnInit(): void {
  }

  private delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }


  async getUsers() {
    console.log("getUsers clicked");
    this.msg = undefined;
    this.users = undefined;
    this._userService.getUsers().subscribe(
      r => {
        this.users = r;
        this.msg = "success";
        console.log("responce : " + r);
      },
      er => {
        this.msg = "error";
        console.log(er);
      },
      () => console.log("getUsers executed")
    );
    let count = 0;
    while (this.users == undefined && count < 25) {
      count++;
      await this.delay(1000);
    }
    console.log("count : " + count);
    console.log("users : " + this.users);
    //if (this.imgObj != "error") {
    //  this.imageNameList = this.users.split("\n");
    //console.log(this.users["Image"]);

    //if (this.users != undefined) {
    //  for (var i of this.users) {
    //    if (i["Image"] != undefined) {
    //      //this.images[count] = i["Image"];
    //      this.images.push(i["Image"]);
    //      console.log(i["Image"]);
    //    }
    //    else {
    //      this.images.push("");
    //    }
    //  }
    //}

    }




}
