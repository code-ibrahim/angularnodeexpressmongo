import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { IUser } from './app/Users';

const baseUrl = 'http://localhost:8080/api/profiles/Id';

@Injectable({
  providedIn: 'root'
})
export class UserRestServiceService {

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get(baseUrl);
  }

  getUsersById(id: string): Observable<IUser[]> {
    console.log("getUsers in user Rest service");
    let temVar = this.http.get<IUser[]>('http://localhost:8080/api/profiles/Id').pipe(catchError(this.errorHandler));;
    //let temVar = this.http.get<IUser>(baseUrl).pipe(catchError(this.errorHandler));;
    console.log("temVar : " + temVar);
    return temVar;
  }

  errorHandler(error: HttpErrorResponse) {
    console.error(error);
    return throwError(error.message || "Server Error");
  } 

}
