module.exports = mongoose => {
    const Tutorial = mongoose.model(
        "tutorial",
        mongoose.Schema(
            {
                //title:String,
                Image: String,
                name: String,
                //id: String
            },
            { timestamps: true }
        )
    );

    return Tutorial;
};